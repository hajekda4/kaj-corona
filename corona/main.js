/**
 * Created by davidhajek on 19.03.2020.
 */

class World {

    constructor() {
        this._OVERLAY_INCREASEMENT = 50;

        this._mapSelector = document.querySelector("#world");
        this._overlaySelector = document.querySelector("#overlay");
        this._goSelector = document.querySelector("#go");
    }

    getHeight() {
        return this._mapSelector.clientHeight;
    }

    getWidth() {
        return this._mapSelector.clientWidth;
    }

    increaseOverlay() {
        let increasedWidth = this._overlaySelector.clientWidth + this._OVERLAY_INCREASEMENT;
        if (increasedWidth >= this.getWidth()) {
            this._overlaySelector.style.width = this.getWidth() + '.px';

            return false;
        }

        this._overlaySelector.style.width = increasedWidth + '.px';

        return true;
    }

    resetOverlay() {
        this._overlaySelector.style.width = 0 + '.px';
        this._goSelector.style.display = 'none';
    }

    showGameOver() {
        this._goSelector.style.display = 'block';
    }
}

class Virus {

    constructor() {
        this._virusSelector = document.querySelector("#virus");
        this._bottom = 0;
        this._left = 0;
        this._width = 80;
        this._height = 80;
        this._state = 'block'; //block, none
    }

    placeSomewhere(width, height) {
        var minBottom = 0;
        var minLeft = 0;
        var maxBottom = height - this._height;
        var maxLeft = width - this._width;

        this._bottom = Math.floor(Math.random() * (maxBottom - minBottom + 1) ) + minBottom;
        this._left = Math.floor(Math.random() * (maxLeft - minLeft + 1) ) + minLeft;

        this._virusSelector.style.bottom = this._bottom + '.px';
        this._virusSelector.style.left = this._left + '.px';
    }

    showVirus() {
        this._state = 'block';
        this._virusSelector.style.display = this._state;
    }

    hideVirus() {
        this._state = 'none';
        this._virusSelector.style.display = this._state;
    }

    isVisible() {
        return (this._state === 'block')
    }
}

class Player {

    constructor(world) {
        this._name = 'Pepa';
        this._world = world;
        this._isOver = false;
    }

    initControl() {

        this._world._virus._virusSelector.addEventListener('mouseenter', e => {
            this._isOver = true;
        });

        this._world._virus._virusSelector.addEventListener('mouseleave', e => {
            this._isOver = false;
        });

        document.addEventListener('keydown', e => {
            if (e.code === 'Space') {
                e.preventDefault();

                var virus = this._world._virus;
                if (virus.isVisible() && this._isOver) {
                    console.log('is down');
                    virus.hideVirus();

                    this._world.updateScore();
                }
            }
        });
    }
}

class Game {

    constructor() {
        this._world = new World();
        this._virus = new Virus();
        this._player = new Player(this);
        this._intervalId = undefined;

        this._virusCount = 0;
        this._missed = 0;

        this._virusCountSelector = document.querySelector("#virusCount");
        this._scoreSelector = document.querySelector("#score");
        this._missedSelector = document.querySelector("#missed");

        this._newGameButton = document.querySelector("#newGame");
        this._stopButton = document.querySelector("#stop");

        console.log('Game created');
    }

    init() {
        // Controls
        this._newGameButton.addEventListener("click", () => {
            this.resetEverything();
            //init player
            this._player.initControl();

            this.play();
            this._intervalId = setInterval(this.play.bind(this), 2000)
        });
        this._stopButton.addEventListener("click", this.stop.bind(this));


        console.log("Game initialization done");
    }

    play() {
        this.increaseVirusCount();
        this._virus.placeSomewhere(this._world.getWidth(), this._world.getHeight());
        this._virus.showVirus();

        setTimeout(() => { //po vterine zmizi
            if (this._virus.isVisible()) {
                this._virus.hideVirus();
                this.increaseMissed();

                if (!this._world.increaseOverlay()) {
                    this._virus.hideVirus();
                    this._world.showGameOver();
                    this.stop();
                }
            }
        }, 1000);
    }

    stop() {
        clearInterval(this._intervalId);
    }

    increaseVirusCount() {
        this._virusCount += 1;
        this._virusCountSelector.innerText = this._virusCount;
    }

    increaseMissed() {
        this._missed += 1;
        this._missedSelector.innerText = this._missed;
        this.updateScore();
    }

    updateScore() {
        this._scoreSelector.innerText = this._virusCount - this._missed
    }

    resetEverything() {
        this.stop();
        this._world = new World();
        this._virus = new Virus();
        this._player = new Player(this);
        this._intervalId = undefined;

        this._virusCount = 0;
        this._missed = 0;
        this._scoreSelector.innerText = this._virusCount - this._missed;
        this._virusCountSelector.innerText = this._virusCount;
        this._missedSelector.innerText = this._missed;
        this._world.resetOverlay();
    }
}
