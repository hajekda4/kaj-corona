/**
 * The main class of this education application.
 *
 * Created by davidhajek on 24.05.2020.
 */
class MainApp {
    /**
     * Creats core parts like controllers container and menu. Takes care od going through memory.
     */
    constructor() {
        this.controllersContainer = new ControllersContainer();
        this.menuComponent = new MenuComponent();


        this.route();
        window.addEventListener('popstate', e => {
            this.route();
            this.menuComponent.processNickname();
        });
    }

    /**
     * Handles routing. Reads the hash, calls the right controller and renders the right section.
     */
    route() {
        let hash = window.location.hash;
        if (hash.includes('?')) hash = hash.substring(0, hash.indexOf('?'));

        this.menuComponent.hash = hash;
        this.controllersContainer.mainsSelector.forEach(page => {
            page.classList.remove("is-visible");
        });
        this.controllersContainer.mainMenusSelector.forEach(menu => {
            menu.classList.remove("active");
        });


        if(this.getNickname() === null) {
            hash = this.controllersContainer.welcome;
        }

        switch(hash) {
            case this.controllersContainer.welcome:
                this.controllersContainer.welcomeSelector.classList.add('is-visible');
                this.controllersContainer.welcomeController.load();
                document.title = "Vítejte";
                this.menuComponent.closeMenu();
                break;
            case this.controllersContainer.dashboard:
                this.controllersContainer.dashboardSelector.classList.add('is-visible');
                this.controllersContainer.dashboardMenuSelector.classList.add('active');
                this.controllersContainer.dashboardController.load();
                document.title = "Přehled";
                this.menuComponent.openMenu();
                break;
            case this.controllersContainer.courses:
                this.controllersContainer.coursesSelector.classList.add('is-visible');
                this.controllersContainer.coursesMenuSelector.classList.add('active');
                this.controllersContainer.coursesController.load();
                document.title = "Kurzy";
                this.menuComponent.openMenu();
                break;
            case this.controllersContainer.detail:
                this.controllersContainer.detailSelector.classList.add('is-visible');
                this.controllersContainer.coursesMenuSelector.classList.add('active');
                this.controllersContainer.detailController.load(this.getQuery('course'), this.getQuery('chapter'));
                document.title = "Kurz";
                this.menuComponent.closeMenu();
                break;
            case this.controllersContainer.info:
                this.controllersContainer.infoSelector.classList.add('is-visible');
                this.controllersContainer.infoMenuSelector.classList.add('active');
                document.title = "Info";
                this.menuComponent.openMenu();
                break;
            case this.controllersContainer.upload:
                this.controllersContainer.uploadSelector.classList.add('is-visible');
                this.controllersContainer.uploadMenuSelector.classList.add('active');
                document.title = "Nový kurz";
                this.menuComponent.openMenu();
                break;
            default:
                if (this.getNickname() === null) {
                    this.controllersContainer.welcomeSelector.classList.add('is-visible');
                    document.title = "Vítejte";
                    this.menuComponent.closeMenu();
                } else {
                    this.controllersContainer.dashboardSelector.classList.add('is-visible');
                    this.controllersContainer.dashboardMenuSelector.classList.add('active');
                    this.controllersContainer.dashboardController.load();
                    document.title = "Přehled";
                    this.menuComponent.openMenu();
                }
        }
    }

    /**
     * Finds the value of url parameter bye key.
     *
     * @param name
     * @returns {*}
     */
    getQuery(name) {
        let hash = window.location.hash;

        if (!hash) return {};
        let url = hash.split("?")[1];

        if (!url) return {};

        let x = url.split("&").reduce(function(result, param) {
            var [key, value] = param.split("=");
            result[key] = value;
            return result;
        }, {});

        return x[name];
    }

    /**
     * Returns nickname or null if it has no nickname.
     *
     * @returns {null}
     */
    getNickname(){
        let nickname = JSON.parse(localStorage.getItem("nickname")) || null;
        return  nickname;
    }
}


/**
 * Handles menu actions.
 */
class MenuComponent {
    /**
     * Creates selectors and sets up listeners.
     */
    constructor() {
        this.hash = '';
        this.menuSelector = document.querySelector("#mainMenu");
        this.toggleButtonSelector = document.querySelector("#toggleMenu");
        this.sectionSelector = document.querySelector("#mainSection");
        this.topbarTextSelector = document.querySelector("#topbar");
        this.deleteSelector = document.querySelector("#deleteEverything");
        this.inputSelector = document.querySelector("#welcome #nickname");

        this.processNickname();

        this.deleteSelector.addEventListener('click', e => this.deleteLocalAndRedirect(e));

        this.toggleButtonSelector.addEventListener('click', e => this.toggleMenu());
    }

    /**
     * Loads nickname for topbar.
     */
    processNickname() {
        this.topbarTextSelector.innerText = JSON.parse(localStorage.getItem("nickname")) || '';
    }

    /**
     * Handles opening of menu.
     */
    openMenu() {
        this.menuSelector.classList.add("openmenu");
        this.sectionSelector.classList.add("openmenu");
        this.toggleButtonSelector.style.marginLeft = '15.6rem';
    }

    /**
     * Handles closing of menu.
     */
    closeMenu() {
        this.menuSelector.classList.remove("openmenu");
        this.sectionSelector.classList.remove("openmenu");
        this.toggleButtonSelector.style.marginLeft = '0.6rem';
    }

    /**
     * Handles closing of menu.
     */
    toggleMenu() {
        if (this.hash === '#welcome' || this.hash === '') return;

        if (this.menuSelector.classList.contains('openmenu')) {
            this.closeMenu()
        } else {
            this.openMenu()
        }
    }

    /**
     * Deletes all localStorages.
     *
     * @param e
     */
    deleteLocalAndRedirect(e) {
        e.preventDefault();

        localStorage.removeItem('courses');
        localStorage.removeItem('nickname');
        localStorage.removeItem('score');
        localStorage.removeItem('log');
        this.inputSelector.value = "";
        window.location.hash = "#welcome";
    }
}

/**
 * Instantiates all controllers. Acts like container.
 */
class ControllersContainer {
    /**
     * Instantiates all controllers and creates selectors.
     */
    constructor() {
        this.welcomeController = new WelcomeController();
        this.dashboardController = new DashboardController();
        this.coursesController = new CoursesController();
        this.detailController = new DetailController();
        this.uploadController = new UploadController();


        this.welcome = '#welcome';
        this.dashboard = '#dashboard';
        this.courses = '#courses';
        this.info = '#info';
        this.detail = '#detail';
        this.upload = '#upload';

        //all
        this.mainsSelector = document.querySelectorAll("section");
        this.mainMenusSelector = document.querySelectorAll("#mainMenu a");

        this.welcomeSelector = document.querySelector("[data-route='#welcome']");
        this.dashboardSelector = document.querySelector("[data-route='#dashboard']");
        this.coursesSelector = document.querySelector("[data-route='#courses']");
        this.infoSelector = document.querySelector("[data-route='#info']");
        this.detailSelector = document.querySelector("[data-route='#detail']");
        this.uploadSelector = document.querySelector("[data-route='#upload']");

        this.welcomeMenuSelector = document.querySelector("#mainMenu a[href='#welcome']");
        this.dashboardMenuSelector = document.querySelector("#mainMenu a[href='#dashboard']");
        this.coursesMenuSelector = document.querySelector("#mainMenu a[href='#courses']");
        this.infoMenuSelector = document.querySelector("#mainMenu a[href='#info']");
        this.detailMenuSelector = document.querySelector("#mainMenu a[href='#detail']");
        this.uploadMenuSelector = document.querySelector("#mainMenu a[href='#upload']");
    }
}


/**
 * Handles welcome.
 */
class WelcomeController {
    /**
     * Creates selectors and sets up listeners.
     */
    constructor() {
        this.buttonSelector = document.querySelector("#welcome .begin");
        this.inputSelector = document.querySelector("#welcome #nickname");
        this.messageSelector = document.querySelector("#welcome .message");
        this.nicknameStorage = "";

        this.load();

        this.buttonSelector.addEventListener("click", e => this.processClick(e));
    }

    /**
     * Loads nickname from local storage.
     */
    load() {
        this.nicknameStorage = JSON.parse(localStorage.getItem("nickname")) || '';
        this.processNewUser();
    }

    /**
     * Checks if you are new or old user.
     */
    processNewUser() {
        if(this.nicknameStorage.length > 0) {
            this.inputSelector.value = this.nicknameStorage;
        }
    }

    /**
     * Processes nickname upload. Validates it.
     * @param e
     */
    processClick(e) {
        e.preventDefault();

        const nickname = this.inputSelector.value;

        if (nickname.length < 1) { //error
            this.messageSelector.innerText = "Zadejte jméno";
            return;
        } else {
            this.messageSelector.innerText = "";
        }

        localStorage.setItem("nickname", JSON.stringify(nickname));
        window.location.href = "#dashboard";
    }
}

/**
 * Controls the dashboard section.
 */
class DashboardController {
    /**
     * Creates selectors and sets up listeners.
     */
    constructor() {
        this.coursesCounterSelector = document.querySelector("#dashboard #coursesCounter");
        this.scoreCounterSelector = document.querySelector("#dashboard #scoreCounter");
        this.buttonChangeSelector = document.querySelector("#dashboard button.changeButton");
        this.inputSelector = document.querySelector("#dashboard #nicknameChange");
        this.messageSelector = document.querySelector("#dashboard .message");
        this.successMessageSelector = document.querySelector("#dashboard .successMessage");
        this.topbarTextSelector = document.querySelector("#topbar");
        this.load();

        this.buttonChangeSelector.addEventListener('click', e => this.changeNickname(e))
    }

    /**
     * Loads dashboard stats from local storage.
     */
    load() {
        this.courses = JSON.parse(localStorage.getItem("courses")) || [];
        this.coursesCounterSelector.innerText = this.courses.length;
        this.score = parseInt(JSON.parse(localStorage.getItem("score")) || 0);
        this.scoreCounterSelector.innerText = this.score;

        this.successMessageSelector.innerText = "";
        this.messageSelector.innerText = "";
    }

    changeNickname(e) {
        e.preventDefault();
        this.successMessageSelector.innerText = "";
        this.messageSelector.innerText = "";

        const nickname = this.inputSelector.value;

        if (nickname.length < 1) { //error
            this.messageSelector.innerText = "Zadejte jméno.";
            return;
        } else {
            this.successMessageSelector.innerText = "Prezdívka byla změněna.";
        }

        localStorage.setItem("nickname", JSON.stringify(nickname));
        this.topbarTextSelector.innerText = nickname;
        this.inputSelector.value = '';
    }
}

/**
 * Controls the courses grid.
 */
class CoursesController {
    /**
     * Creates selectors.
     */
    constructor() {
        this.coursesContainerSelector = document.querySelector("#courses #coursesContainer");
        this.load()
    }

    /**
     * Loads courses.
     */
    load() {
        this.coursesContainerSelector.innerHTML = "";

        this.courses = JSON.parse(localStorage.getItem("courses")) || [];
        this.courses.forEach(c => {
            let a = document.createElement('a');
            a.setAttribute('href', '#detail?course=' + c.id);
            let div = document.createElement('div');
            let span = document.createElement('span');
            let i = document.createElement('i');
            if (c.type === 'game') {
                i.setAttribute('class', 'fas fa-gamepad');
                i.style.color = '#0061f2'
            } else {
                i.setAttribute('class', 'fas fa-graduation-cap');
                i.style.color = 'rgba(105, 0, 199, 0.8)'
            }

            span.innerText = c.name;
            div.appendChild(i);
            a.appendChild(div);
            a.appendChild(span);
            this.coursesContainerSelector.appendChild(a);
        });
    }
}

/**
 * Controls the presentation section.
 */
class DetailController {
    /**
     * Creates selectors.
     */
    constructor() {
        this.detailContainerSelector = document.querySelector("#detail #detailContainer");
        this.detailCourseName = document.querySelector("#detail #detailCourseName");
    }

    /**
     * Load course and its chapter. It uses Presentation class.
     *
     * @param id
     * @param chapterIndex
     */
    load(id, chapterIndex) {
        this.detailContainerSelector.innerHTML = "";
        this.course = this.getCourse(id);

        if (this.course === null) {
            let div = document.createElement('div');
            div.setAttribute('class', 'danger');
            div.innerText = "Kurz neexistuje";
            this.detailContainerSelector.appendChild(div);
            return;
        }

        this.detailCourseName.innerText = this.course.name;

        new Presentation(this.course, chapterIndex, id);
    }

    /**
     * Finds course with id.
     *
     * @param id
     * @returns {*}
     */
    getCourse(id) {
        const courses = JSON.parse(localStorage.getItem("courses")) || [];
        let course = null;
        courses.forEach(c => {
            if(c.id == id) { //compering number and string, therefore ==
                course = c;
            }
        });

        return course;
    }
}

/**
 * Controls upload section.
 */
class UploadController {
    /**
     * Creates selectors and sets up listeners.
     */
    constructor() {
        this.dndSelector = document.querySelector("#dnd");
        this.errorMessageSelector = document.querySelector("#upload .errorMessage");
        this.successMessageSelector = document.querySelector("#upload .successMessage");
        this.nicknameStorage = "";
        this.modalVisible = false;
        this.modalButtons = document.querySelectorAll('.modal-button');
        this.modal = document.querySelector('.modal');


        for (let i = 0; i < this.modalButtons.length; i++) {
            this.modalButtons[i].addEventListener('click', e => this.toggleModalState())
        }

        this.dndSelector.addEventListener("drop", e => this.uploadCourse(e));
        this.dndSelector.addEventListener("dragover", e => e.preventDefault());
    }

    /**
     * Handles json file upload.
     *
     * @param e
     */
    uploadCourse(e) {
        e.preventDefault();
        this.errorMessageSelector.innerText = "";
        this.successMessageSelector.innerText = "";
        this.courses = JSON.parse(localStorage.getItem("courses")) || [];

        const files = e.dataTransfer.files;
        const fr = new FileReader();


        for (const f of files) {
            fr.addEventListener("load", e => {
                let error = false;

                if(f.type !== 'application/json'){
                    this.errorMessageSelector.innerText += 'Soubor ' + f.name + ' musí být datového typu JSON. ';
                    error = true;
                    return;
                }

                let course = JSON.parse(e.target.result);

                this.courses.forEach(c => {
                    if(c.id === course.id || c.name === course.name) {
                        this.errorMessageSelector.innerText += 'Soubor ' + f.name + ' už je jednou nahraný. ';
                        error = true;
                    }
                });


                if (!error){
                    this.courses.push(course);
                    localStorage.setItem("courses", JSON.stringify(this.courses));
                    this.successMessageSelector.innerText += 'Soubor ' + f.name + ' byl úspěšně nahrán. ';
                }
            });

            fr.readAsText(f);
        }
    }

    /**
     * Toggles modal states.
     */
    toggleModalState() {
        this.modalVisible = !this.modalVisible;
        if (this.modalVisible) {
            document.body.classList.add('modal-visible');
        } else {
            document.body.classList.remove('modal-visible');
        }
    }
}


/**
 * Presentation class represents logic behind slide presentations for courses.
 */
class Presentation {
    /**
     * Creates selectors and sets up listeners.
     *
     * @param course
     * @param chapterIndex
     * @param courseId
     */
    constructor(course, chapterIndex, courseId) {
        this.course = course;
        this.chapters = course.chapters;
        this.n = chapterIndex != null ? parseInt(chapterIndex-1, 10) : 0;

        this.detailContainerSelector = document.querySelector("#detail #detailContainer");
        this.detailProgressSelector = document.querySelector("#detail #progress");

        this.prependArrows();
        this.createSlides();

        this.prevButtonSelector = document.querySelector("a[target='prev']");
        this.nextButtonSelector = document.querySelector("a[target='next']");
        this.slidesSelector = document.querySelectorAll('#detail .slide');


        this.prevButtonSelector.addEventListener('click', e => {
            e.preventDefault();
            this.n = (this.n - 1) >= 0 ? this.n - 1 : this.slidesSelector.length -1;
            this.showSlide(this.n, courseId)
        });
        this.nextButtonSelector.addEventListener('click', e => {
            e.preventDefault();
            this.n = (this.n + 1) < this.slidesSelector.length ? this.n + 1 : 0;
            this.showSlide(this.n, courseId)
        });

        this.showSlide(this.n, courseId)
    }

    /**
     * Shows specific slide with orde n for course with id courseId.
     *
     * @param n
     * @param courseId
     */
    showSlide(n, courseId) {
        var i;
        for (i = 0; i < this.slidesSelector.length; i++) {
            this.slidesSelector[i].classList.remove('visible');
            if (i === n) {
                this.slidesSelector[i].classList.add('visible');
            }
        }

        this.detailProgressSelector.style.width = ((n+1) / this.slidesSelector.length * 100) + '%';
        window.location.hash = "#detail?course=" + courseId + "&chapter=" + (n+1);
    }

    /**
     * Creates slides
     */
    createSlides() {
        this.chapters.forEach(ch => {
            let div = document.createElement('div');
            div.setAttribute('class', 'slide');
            let h2 = document.createElement('h2');
            h2.innerText = ch.name;
            let ul = document.createElement('ul');

            ch.content.forEach(con => {
                let li = document.createElement('li');
                li.innerText = con;
                ul.appendChild(li);
            });

            div.appendChild(h2);
            div.appendChild(ul);

            this.detailContainerSelector.appendChild(div);
        });


        //add quiz or gamesu
        if (this.course.type === 'game') {
            new Gamesa(this.course, this.detailContainerSelector)
        } else if (this.course.type === 'quiz') {
            new Quiz(this.course, this.detailContainerSelector);
        }
    }

    /**
     * Creates and prepends arrows to navigate through slide presentation.
     */
    prependArrows(){
        let left = document.createElement('i');
        let right = document.createElement('i');
        left.setAttribute('class', 'fas fa-arrow-left');
        right.setAttribute('class', 'fas fa-arrow-right');
        let prev = document.createElement('a');
        let next = document.createElement('a');
        prev.setAttribute('class', 'prev');
        next.setAttribute('class', 'next');
        prev.setAttribute('target', 'prev');
        next.setAttribute('target', 'next');
        prev.appendChild(left);
        next.appendChild(right);

        this.detailContainerSelector.appendChild(prev);
        this.detailContainerSelector.appendChild(next);
    }
}

/**
 * Quiz represents logic behind small quizzes in slide presentation.
 */
class Quiz{
    /**
     * Constructor which creates most of the html for Quiz..
     *
     * @param course
     * @param detailContainerSelector
     */
    constructor(course, detailContainerSelector) {
        let div = document.createElement('div');
        div.setAttribute('class', 'slide');
        div.setAttribute('id', 'quizSlide');
        let h2 = document.createElement('h2');
        h2.innerText = "Kvíz";
        let ul = document.createElement('ul');
        ul.style.listStyle = 'none';
        let button = document.createElement('button');
        button.setAttribute('id', 'correctTest');
        button.style.marginTop = '15px';
        button.innerText = "opravit";
        let questions = course.questions;
        questions.forEach(que => {
            let li = document.createElement('li');
            let input = document.createElement('input');
            let label = document.createElement('label');
            label.innerText = que.question + "?";
            input.setAttribute('class', 'qna');
            input.setAttribute('question', que.question);
            input.setAttribute('answer', que.answer);

            li.appendChild(label);
            li.appendChild(input);
            ul.appendChild(li);
        });

        let li = document.createElement('li');
        li.appendChild(button);
        ul.appendChild(li);
        div.appendChild(h2);
        div.appendChild(ul);
        detailContainerSelector.appendChild(div);


        this.correctButtonSelector = document.querySelector("#detail #correctTest");
        this.inputsSelector = document.querySelectorAll("#detail .qna");

        this.correctButtonSelector.addEventListener('click', e => this.correctTest(e));
    }


    /**
     * Corrects the test, shows result and saves it to localStorage.
     *
     * @param e
     */
    correctTest(e) {
        e.preventDefault();
        let score = 0;

        this.inputsSelector.forEach(inp => {
           if (inp.value == inp.getAttribute('answer')) {
               score += 2;
               inp.style.borderColor = '#1cc88a';
               inp.style.borderStyle = 'solid';
               inp.style.borderwidth = '1.5px';
           } else {
               inp.style.borderColor = '#e81500';
               inp.style.borderStyle = 'solid';
               inp.style.borderwidth = '1.5px';
               inp.value += " spravně je " + inp.getAttribute('answer');
           }
        });

        //inform user
        let quizSlide = document.querySelector("#detail #quizSlide");
        let div = document.createElement('div');
        let a = document.createElement('a');
        a.setAttribute('href', '#dashboard');
        a.style.color = '#007bff';
        a.innerText = 'Zpět na přehled';
        let span = document.createElement('span');
        span.setAttribute('class', 'success');
        span.innerText = "Gratuluji! Dosáhli jste skóre " + score + "...:-) ";

        div.appendChild(span);
        div.appendChild(a);
        quizSlide.appendChild(div);

        //refresh localStorage
        let sc = parseInt(JSON.parse(localStorage.getItem("score")) || 0);
        sc += parseInt(score);
        localStorage.setItem("score", JSON.stringify(sc));

        //avoid double send
        this.correctButtonSelector.style.display = 'none';
    }
}

/**
 * Gamesa class contains the logic behind the small games in slide presentation.
 */
class Gamesa{
    /**
     * Creates basic html canvas and globals of the game.
     *
     * @param course
     * @param detailContainerSelector
     */
    constructor(course, detailContainerSelector) {
        let divForButton = document.createElement('div');
        let div = document.createElement('div');
        div.setAttribute('class', 'slide');
        div.setAttribute('id', 'gamesSlide');
        let button = document.createElement('button');
        button.setAttribute('id', 'startGame');
        button.style.marginTop = '15px';
        button.style.marginBottom = '15px';
        button.innerText = "Spustit hru!";
        divForButton.appendChild(button);
        div.appendChild(divForButton);
        detailContainerSelector.appendChild(div);

        let buttonSelector = document.querySelector('#startGame');
        buttonSelector.addEventListener("click", e => this.startGame(e));
    }


    startGame() {
        console.log('ahoj');
        this.timestampStart = null;
        let gameMessage = document.getElementById('gameMessage');
        if (gameMessage !== null) gameMessage.parentNode.removeChild(gameMessage);
        let canvaToDelete = document.getElementById('canvaIdentifier');
        if (canvaToDelete !== null) canvaToDelete.parentNode.removeChild(canvaToDelete);


        let div = document.querySelector('#detail #gamesSlide');
        let canva = document.createElement('canvas');
        canva.setAttribute('id', 'canvaIdentifier');
        canva.setAttribute('width', '800');
        canva.setAttribute('height', '400');
        div.appendChild(canva);

        this.speedMultiplier = 0.1;
        const canvas = document.querySelector('canvas');
        this.ctx = canvas.getContext('2d');
        this.WIDTH = canvas.offsetWidth;
        this.HEIGHT = canvas.offsetHeight;
        this.backgroundImg = new Image();
        this.backgroundImg.src = "bcmathres.jpg";
        this.calcImg = new Image(); // size 128x128
        this.calcImg.src = "calculator2.png";
        this.victory = new Image();
        this.victory.src = "starwars.jpg";
        this.soundExplosion = new Audio("explosion.mp3");
        this.soundMiss = new Audio("miss.mp3");

        // game state
        this.start = null;
        this.startOfEnd = null;
        this.end = false;
        this.calculators = [
            { x: 30, y:this.HEIGHT, speed: 1.0 },
            { x: 80, y:this.HEIGHT, speed: 0.5 },
            { x:150, y:this.HEIGHT, speed: 0.3 },
            { x:200, y:this.HEIGHT, speed: 0.6 },
            { x:300, y:this.HEIGHT, speed: 0.8 },
            { x:450, y:this.HEIGHT, speed: 2.0 },
            { x:600, y:this.HEIGHT, speed: 1.0 },
            { x:700, y:this.HEIGHT, speed: 0.3 },
        ];

        canvas.addEventListener("click", e => this.handleClick(e));

        this.animationFramId = requestAnimationFrame(this.step.bind(this));
    }


    /**
     * Handles click inside canvas.
     *
     * @param e
     */
    handleClick(e) {
        this.soundMiss.pause();
        this.soundMiss.currentTime = 0;
        this.soundExplosion.pause();
        this.soundExplosion.currentTime = 0;

        const x = e.offsetX;
        const y = e.offsetY;
        this.calculators = this.calculators.filter(calculator => {
            if ((x >= calculator.x && x <= calculator.x + this.calcImg.width) &&
                (y >= calculator.y && y <= calculator.y + this.calcImg.height)
            ) {
                this.soundExplosion.play();
                return false;
            } else {
                this.soundMiss.play();
                return true;
            }
        });
    }

    /**
     * Draws image of calculator inside canvas.
     *
     * @param calculator
     * @param dt
     */
    drawCalculator(calculator, dt) {
        if (calculator.y < -this.calcImg.height) {
            calculator.y = this.HEIGHT;
        } else {
            calculator.y -= dt * calculator.speed * this.speedMultiplier;
        }
        this.ctx.drawImage(this.calcImg, calculator.x, calculator.y);
    }

    /**
     * Rotates victory img.
     *
     * @param angle
     * @param positionX
     * @param positionY
     */
    rotateAndDrawImage (angle , positionX, positionY) {
        this.ctx.translate( positionX, positionY );
        this.ctx.rotate( Math.PI/180*angle );
        this.ctx.drawImage(this.victory, -100, -150, 180, 279);
        this.ctx.rotate( -Math.PI/180*angle );
        this. ctx.translate( -positionX, -positionY );
    }

    /**
     * One step of animation frame. It calls itself recursively.
     *
     * @param timestamp
     */
    step(timestamp) {
        if (this.timestampStart === null) this.timestampStart = timestamp;
        if (!this.start) {
            this.start = timestamp;
        }
        const dt = timestamp - this.start;
        this.start = timestamp;

        this.ctx.clearRect(0, 0, this.WIDTH, this.HEIGHT);
        this.ctx.drawImage(this.backgroundImg, 0, 0);

        this.calculators.forEach(calculator => {
            this.drawCalculator(calculator, dt);
        });

        if (this.calculators.length > 0) {
            requestAnimationFrame(this.step.bind(this));
        } else {
            if (!this.startOfEnd) this.startOfEnd = timestamp;
            var progress = timestamp - this.startOfEnd;

            if (progress < 2150) {
                requestAnimationFrame(this.step.bind(this));
                this.rotateAndDrawImage(progress / 3, 400, 200)
            } else {
                this.end = true;
                this.renderScore(timestamp-2150);

                this.ctx.drawImage(this.victory, 300, 50);
            }
        }
    }

    /**
     * Renders the outcome of the game.
     *
     * @param timestemp
     */
    renderScore(timestemp){
        let sec = parseInt((timestemp - this.timestampStart)/1000);
        let score = 23 - sec;

        let gamesSlide = document.querySelector("#detail #gamesSlide");
        let div = document.createElement('div');
        div.setAttribute('id', 'gameMessage');
        let a = document.createElement('a');
        a.setAttribute('href', '#dashboard');
        a.style.color = '#007bff';
        a.innerText = 'Zpět na přehled';
        let span = document.createElement('span');
        if (score > 0) {
            span.setAttribute('class', 'success');
            span.innerText = "Gratuluji! Dosáhli jste skóre " + score + "...:-) Čas byl " + sec + " vteřin. ";
        } else {
            span.setAttribute('class', 'danger');
            span.innerText = "Bohužel jste nebyli dostatečně dobří a nebo jste hrali znovu. Získávate skóre 0. ";
        }

        div.appendChild(span);
        div.appendChild(a);
        gamesSlide.appendChild(div);

        //refresh localStorage
        if (score > 0) {
            let sc = parseInt(JSON.parse(localStorage.getItem("score")) || 0);
            sc += parseInt(score);
            localStorage.setItem("score", JSON.stringify(sc));
        }

        if (this.animationFramId) cancelAnimationFrame(this.animationFramId);
    }
}






new MainApp();
